<?php
/**
 * @file
 * Votesane Map.
 */

/**
* Implements hook_block_info.
*/
function votesane_map_block_info() {
  $block = array();
  $blocks['votesane_map'] = array('info' => t('Votesane Map'));
  return $blocks;
}

/**
* Implements hook_block_view.
*/
function votesane_map_block_view($delta = '') {
  $block = array();
  switch ($delta) {
    case 'votesane_map':
      global $base_url;
      $classes = _votesane_map_get_state_parties();
      $rel_module_path = base_path() . drupal_get_path('module', 'votesane_map');
      $module_path = $base_url . $rel_module_path;
      drupal_add_js($module_path . '/js/d3.v3.min.js');
      drupal_add_js($module_path . '/js/queue.min.js');
      drupal_add_js($module_path . '/js/topojson.min.js');
      drupal_add_js($module_path . '/js/d3.tip.v0.6.3.js');
      drupal_add_js($module_path . '/js/votesane-map.js', array('scope' => 'footer'));
      drupal_add_js(array('module_path' => $rel_module_path), 'setting');
      drupal_add_js(array('class_arr' => $classes), 'setting');
      drupal_add_js(array('search_path' => '/candidate-search/state/'), 'setting');
      drupal_add_js(array('display_tip' => TRUE), 'setting');
      $block['content'] = '<div id="map-container"></div>';
      break;

    default:
      break;
  }
  return $block;
}

/**
 * Helper function to get counts of the party member in each state.
 */
function _votesane_map_count_party_in_state($party) {
  $query = db_select('field_data_field_party', 'fp')->fields('fp');
  $query->leftJoin('field_data_field_state', 'fs', 'fp.entity_id = fs.entity_id');
  $query->fields('fs', array('field_state_value'));
  $query->addExpression('COUNT(fp.entity_id)', 'party_count');
  $query->condition('fp.field_party_value', $party)->condition('fs.field_state_value', '', '!=')->isNotNull('fs.field_state_value');
  $query->groupBy('fs.field_state_value');
  $result = $query->execute()->fetchAll();
  $return = array();
  foreach ($result as $value) {
    $state = drupal_strtolower($value->field_state_value);
    $party = drupal_strtolower($value->field_party_value);
    $return[$state] = $value->party_count;
  }
  return $return;
}

/**
 * Helper function to get parties with each state.
 */
function _votesane_map_get_state_parties() {
  $rep_count = _votesane_map_count_party_in_state('Republican');
  $dem_count = _votesane_map_count_party_in_state('Democrat');
  $ind_count = _votesane_map_count_party_in_state('Independent');
  $states = _votesane_basic_settings_state_list();
  $classes = array();
  foreach ($states as $state) {
    $value = str_replace(' ', '_', strtolower($state));
    $state = drupal_strtolower($state);
    $state = str_replace(' ', '_', $state);
    $count_array = array();
    $count_array['republican'] = (isset($rep_count[$state])) ? $rep_count[$state] : NULL;
    $count_array['democrat'] = (isset($dem_count[$state])) ? $dem_count[$state] : NULL;
    $count_array['independent'] = (isset($ind_count[$state])) ? $ind_count[$state] : NULL;
    $party_max = array_keys($count_array, max($count_array));
    $max_party = (isset($party_max[0])) ? $party_max[0] : '';
    $classes[$state] = variable_get('votesane_search_state_' . $value . '_party', $max_party);
  }
  return $classes;
}
