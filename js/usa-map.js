/**
 * @file
 * To populate responsive map.
 */

var module_path = Drupal.settings.module_path;
var search_path = Drupal.settings.search_path;
var display_tip = Drupal.settings.display_tip;
var class_arr = Drupal.settings.class_arr;

var url = module_path + "/js/us.json";

var margin = {top: 10, left: 0, bottom: 10, right: 0}
  , width = parseInt(d3.select('#map-container').style('width'))
  , width = width - margin.left - margin.right
  , mapRatio = .8
  , height = width * mapRatio;

var formats = {
  percent: d3.format('%')
};

// projection and path setup
var projection = d3.geo.albersUsa().scale(width).translate([width/2 , height/2]);

var path = d3.geo.path().projection(projection);

// make a map
var map = d3.select('#map-container').append('svg')
  .style('height', height + 'px')
  .style('width', width + 'px');

if (display_tip) {
  var tip = d3.tip()
    .attr('class', 'd3-tip')
    .offset([-10, 0])
    .html(function(d) {
      return "<span class='state-name'>" + d.properties.name + "</span>";
    });
    map.call(tip);
}

// queue and render
queue()
  .defer(d3.json, url)
  .await(render);

function render(err, us) {
  var land = topojson.mesh(us, us.objects.land)
    , states = topojson.feature(us, us.objects.states);  
  window.us = us;
  map.append('path')
    .datum(land)
    .attr('class', 'land')
    .attr('d', path);
  
  var states = map.selectAll('path.state')
    .data(states.features)
    .enter().append('a')
      .attr('class', 'state-wrapper')
      .attr('xlink:href', function(d) { 
        return search_path + d.properties.name.toLowerCase().replace(/\s/g, '+'); 
      })
      .append('path')
        .attr('class', function(d) {
          var state = d.properties.name.toLowerCase();
          var state_val = state.replace(/\s/g, '_');
          var party_class = '';
          if (state_val in class_arr) {
            party_class = ' ' + class_arr[state_val];
          }
          return 'state ' + state.replace(/\s/g, '-') + party_class;
        })
        .attr('id', function(d) { 
          return d.properties.name.toLowerCase().replace(/\s/g, '-'); 
        })
        .attr('d', path);
  if (display_tip) {
    states.on('mouseover', tip.show).on('mouseout', tip.hide);
  }
}

// catch the resize
d3.select(window).on('resize', resize);

function resize() {
  // adjust things when the window size changes
  width = parseInt(d3.select('#map-container').style('width'));
  width = width - margin.left - margin.right;
  height = width * mapRatio;

  // update projection
  projection
    .translate([width / 2, height / 2])
    .scale(width);

  // resize the map container
  map
    .style('width', width + 'px')
    .style('height', height + 'px');

  // resize the map
  map.select('.land').attr('d', path);
  map.selectAll('.state').attr('d', path);
}
